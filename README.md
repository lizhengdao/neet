[![](https://jitpack.io/v/com.gitlab.technowolf/neet.svg)](https://jitpack.io/#com.gitlab.technowolf/neet)
## Neet
Easy to use network observer built with kotlin!

## Features
- Realtime updates for network changes.
- Tiny in size.
- Provides thread safe updates, Thanks to Kotlin Coroutines.
- Permits to cancel updates whenever needed.
- Choose between single value or LiveData to observe from your views.

## Installation
Add the JitPack repository to your build file 

``` groovy
allprojects {
    repositories {
        maven { url 'https://jitpack.io' }
    }
}
```

Add the dependency
``` groovy
implementation 'com.gitlab.technowolf:neet:$version' //Replace $version it with latest version
```


## Getting Started

Start NeetEngine in your application class.

``` kotlin
class YourApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        //Start NeetEngine!
        NeetEngine.attachNetworkObserver(this, NeetObserver.HighPriority)
    }
}
```

**NeetObserver priorities**

| Priority | Delay between each update |
| ------ | ------ |
| LowPriority | 30 seconds |
| MediumPriority | 15 seconds |
| HighPriority | 5 seconds |
| Realtime | 1 seconds |


Then, Use it in your views.

```kotlin
val result = isInternetConnected()

// Or if you prefer live data

isInternetConnectedLiveData().observe(viewLifecycleOwner, Observer {
    when (it) {
        true -> {
            //Network available
        }
        false -> {
            //Network unavailable
        }
    }
})
```

Finally, If you want to detach network observer,
```kotlin
detachNetworkObserver()
```

## License

[MIT License](LICENSE) Copyright (c) 2020 TechnoWolf FOSS

Neet is provided under terms of MIT license.

## Links

[Issue Tracker](https://gitlab.com/technowolf/neet/issues)