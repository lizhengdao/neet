/*
 * MIT License
 *
 * Copyright (c) 2020.  TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package `in`.technowolf.neet

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class NeetEngine private constructor() {

    suspend fun attachNetworkObserver(applicationContext: Application, neetObserver: NeetObserver) {
        val isNetworkAvailable = isNetworkAvailable(applicationContext)
        _networkStateLiveData.postValue(isNetworkAvailable)
        networkState = isNetworkAvailable
        when (neetObserver) {
            NeetObserver.LowPriority -> delay(LOW_PRIORITY)
            NeetObserver.MediumPriority -> delay(MEDIUM_PRIORITY)
            NeetObserver.HighPriority -> delay(HIGH_PRIORITY)
            NeetObserver.Realtime -> delay(REALTIME_PRIORITY)
            is NeetObserver.CustomPriority -> delay(neetObserver.customInterval)
        }
        attachNetworkObserver(applicationContext, neetObserver)
    }

    private fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val networkCapabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (networkCapabilities != null) {
                when {
                    networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> return true
                    networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> return true
                    networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> return true
                }
            }
        } else {
            return connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo!!.isConnected
        }
        return false
    }

    companion object {

        private const val LOW_PRIORITY: Long = 30 * 1000
        private const val MEDIUM_PRIORITY: Long = 15 * 1000
        private const val HIGH_PRIORITY: Long = 5 * 1000
        private const val REALTIME_PRIORITY: Long = 1 * 1000

        private var applicationContext: Application? = null

        private var _networkStateLiveData: MutableLiveData<Boolean> = MutableLiveData()
        val networkStateLiveData: LiveData<Boolean> = _networkStateLiveData

        private var networkState: Boolean = false

        private var coroutineScope: CoroutineContext = Dispatchers.IO + Job()

        fun isConnected(): Boolean {
            return if (applicationContext == null) {
                throw Exception("Initialization is not done from Application class!")
            } else {
                networkState
            }
        }

        fun isConnectedLiveData(): LiveData<Boolean> {
            return if (applicationContext == null) {
                throw Exception("Initialization is not done from Application class!")
            } else {
                networkStateLiveData
            }
        }

        fun attachNetworkObserver(applicationContext: Application, neetObserver: NeetObserver) {
            val engineInstance = NeetEngine()
            this.applicationContext = applicationContext
            coroutineScope.let {
                CoroutineScope(it).launch {
                    engineInstance.attachNetworkObserver(applicationContext, neetObserver)
                }
            }
        }

        fun detachObserver() {
            if (applicationContext == null) {
                throw Exception("Initialization is not done from Application class!")
            } else {
                coroutineScope.cancel(CancellationException("Network observer job cancelled."))
            }
        }
    }
}